Feature: Login 

        Different users see different things?

        Background:
                Given I am on loginpage

        Scenario: Login with all roles
                When I login with 'al.roles@bosa.fgov.be' and 'D3faultFDM'
                Then I should see the dashboard

        Scenario: Login with Albert
                When I login with 'albert.theo@bosa.fgov.be' and 'D3faultFDM'
                Then I should see the dashboard