<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverWait;
use Facebook\WebDriver\WebDriverExpectedCondition;

class FeatureContext implements Context, SnippetAcceptingContext
{
    protected $driver;
    protected $wait;
    protected $cucumbers;
    protected $cucumbersLeft;

    public function __construct()
    {
    }

    /**
     * @Given I am on homepage
     */
    public function iAmOnHomepage()
    {
        require 'pages\Homepage.php';
        $this->driver = RemoteWebDriver::create('http://localhost:8000/wd/hub', DesiredCapabilities::chrome());
        $this->driver->get('http://bing.com');
        $this->driver->manage()->window()->maximize();
        $this->wait = new WebDriverWait($this->driver, 10);
        $this->wait->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::name($searchName)));
    }

    /**
     * @When I search for cheese
     */
    public function iSearchForCheese()
    {
        require 'pages\Homepage.php';
        $searchbox = $this->driver->findElement(WebDriverBy::name($searchName));
        $searchbox->sendKeys('Cheese');
        $searchbox->submit();
    }

    /**
     * @Then I get results
     */
    public function iGetResults2()
    {
        require 'pages\Homepage.php';
        $this->wait->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id($resultsId)));
        $this->driver->quit();
    }

    /**
     * @Given there are :arg1 cucumbers
     */
    public function thereAreCucumbers($arg1)
    {
        $this->cucumbers = $arg1;
    }

    /**
     * @When I eat :arg1 cucumbers
     */
    public function iEatCucumbers($arg1)
    {
        $this->cucumbersLeft = $this->cucumbers - $arg1;
    }

    /**
     * @Then I should have :arg1 cucumbers
     */
    public function iShouldHaveCucumbers($arg1)
    {
        if($this->cucumbersLeft != $arg1){
            throw new Exception('Expected: ' . $this->cucumbersLeft . ' but got: ' . $arg1);
        }
    }

    /**
     * @Given I am on loginpage
     */
    public function iAmOnLoginpage()
    {
        require 'pages\Loginpage.php';
        $this->driver = RemoteWebDriver::create('http://localhost:8000/wd/hub', DesiredCapabilities::chrome());
        $this->driver->manage()->window()->maximize();
        $this->driver->get('https://evalfed-test.crescendo.belgium.be/login');
        $this->wait = new WebDriverWait($this->driver, 10);
        $this->wait->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($loginBtnXpath)));
        $this->driver->findElement(WebDriverBy::xpath($loginBtnXpath))->click();
    }

    /**
     * @When I login with :arg1 and :arg2
     */
    public function iLoginWithAnd($email, $pw)
    {
        require 'pages\Loginpage.php';
        $this->wait->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id($emailId)));
        $this->driver->findElement(WebDriverBy::id($emailId))->sendKeys($email);
        $this->driver->findElement(WebDriverBy::id($pwId))->sendKeys($pw)->submit();

        //accept GDPR screen
        $this->wait->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id($gdprBtnId)));
        $this->driver->findElement(WebDriverBy::id($gdprBtnId))->click();
    }

    /**
     * @Then I should see the dashboard
     */
    public function iShouldSeeTheDashboard()
    {
        require 'pages\Dashboard.php';
        $this->wait->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id($dashboardId)));
        $this->driver->quit();
    }
}
