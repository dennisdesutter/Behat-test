Feature: Search

US1.    As a user 
        I want to search for something
        and get related results
         
         #Background: When multiple scenarios have the same first step, you can use this to run before each scenario
         #       Given I am on the homepage

        Scenario: Search for cheese
                Given I am on homepage
                When I search for cheese
                Then I get results

        Scenario Outline: eating
                Given there are <start> cucumbers
                When I eat <eat> cucumbers
                Then I should have <left> cucumbers

                Examples:
                | start | eat | left |
                |    12 |   5 |    7 |
                |    20 |   5 |   15 |